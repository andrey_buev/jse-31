package ru.buevas.jse.service;

import java.math.BigInteger;

public class FactorialThread extends Thread {

    private FactorialPart part;
    private BigInteger result;

    public FactorialThread(FactorialPart part) {
        this.result = BigInteger.ONE;
        this.part = part;
    }

    @Override
    public void run() {
        for (int i = part.getBegin(); i <= part.getEnd(); i++) {
            result = result.multiply(i == 0 ? BigInteger.ONE : BigInteger.valueOf(i));
        }
    }

    public BigInteger getResult() {
        return result;
    }
}
