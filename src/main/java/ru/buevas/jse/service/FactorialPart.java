package ru.buevas.jse.service;

public class FactorialPart {
    private int begin;
    private int end;

    public FactorialPart(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "FactorialPart{" +
                "begin=" + begin +
                ", end=" + end +
                '}';
    }
}
